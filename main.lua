
local state = require("src.state")
local menuState = require("src.states.menuState")
local gameState = require("src.states.gameState")
local inventoryState = require("src.states.inventoryState")
local splashScreenState = require("src.states.splashScreenState")

local currentState
local stateReturn

local Game
local Menu
Casual_game_track= love.audio.newSource( "/assets/music/Casual_game_track.ogg","stream" )
				love.audio.play( Casual_game_track )
				Casual_game_track:setVolume(0.4)
				Casual_game_track:setEffect('myEffect')
				Casual_game_track:setLooping( true )

require("loadAssets")
WebP = require("love-webp")			-- webp library for love

local paused = false


local success, error_message = pcall(function()
    joystick = love.joystick.getJoysticks()[1]
    require("gamepadinput")
    loadcontrollermappings()
    gamepadTimer = 0
end)

if not success then
    print("Error:", error_message)
end

function love.load()

  --love.graphics.setBackgroundColor( 53/2 , 99/2 ,33/2)
  --love.graphics.setBackgroundColor( 0 , 0.6 ,0)
  currentState = state.create(splashScreenState,false)

  Game = state.create(gameState)
  Inv = state.create(inventoryState)

end

function love.update(dt)
	isjoystickbeingpressed(joystick,button)
	updategetjoystickaxis(dt)
  

  if currentState == Inv then
    stateReturn = currentState:update(Game)
  else
    stateReturn = currentState:update()
  end

  if stateReturn == 1 then
    currentState = Game
    paused = false
  end

  if stateReturn == 2 then
    currentState = state.create(menuState,false)
    paused = false
  end


  if love.keyboard.isDown( "escape" ) and currentState == Game then
     currentState = state.create(menuState,true)
     paused = true
  end

  if love.keyboard.isDown( "tab" ) and currentState == Game and stateReturn ~= 1 then
     currentState = Inv
     paused = true
  end
end


function isjoystickbeingpressed(joystick,button)
if joystick then
		if joystick:isGamepadDown("a") then
				
	elseif joystick and joystick:isGamepadDown("back") then
				currentState = Inv
				paused = true
	elseif joystick and joystick:isGamepadDown("x") then
	
	elseif joystick and joystick:isGamepadDown("y") then
	
	elseif joystick and joystick:isGamepadDown("start") then		
				currentState = state.create(menuState,true)
				paused = true	
	
	else
	
	
	end
end
end


function love.draw()
  if paused then
    Game:draw()
  end
  currentState:draw()
end
