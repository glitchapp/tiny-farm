local splashScreenState = {}

local backgroundImage
backgroundOffset = 0

splashScreenState.load = function(self)
  backgroundImage = loadAsset("assets/img/menu/menuBG.webp", "Image")
  --SplashImage = love.graphics.newImage("assets/img/menu/splashScreen.png")
  SplashImage = loadAsset("assets/img/menu/splashScreen.webp", "Image")
  
  backgroundBarImage= love.graphics.newImage("assets/img/menu/menuSideBar.png")
  
  self.timeout = 350
end

splashScreenState.update = function(self)

  if backgroundOffset > -4096 then
    backgroundOffset = backgroundOffset - 1
  else
    backgroundOffset = 0
  end

  self.timeout = self.timeout - 1
  local returnValue = 0
  if self.timeout < 0 then
    returnValue = 2
    love.graphics.setColor(1,1,1,1)
  end
  return returnValue
end

splashScreenState.draw = function(self)

  --love.graphics.printf("SplashScreen",0, 40, love.graphics.getWidth(),"center")
  if self.timeout<250 then 
	love.graphics.setColor(1,1,1,1-(self.timeout/150))
	else
	love.graphics.setColor(1,1,1,0)
  end
  for x=0,love.graphics.getWidth(),128*8 do
	love.graphics.draw(backgroundImage, x+backgroundOffset,y,0,1,1)
	love.graphics.draw(backgroundBarImage,-200,0)

	end
	if self.timeout<250 then 
		love.graphics.setColor(1,1,1,(self.timeout/150))
	else
		love.graphics.setColor(1,1,1,(-self.timeout+350)/10)	
	end
  love.graphics.draw(SplashImage,(love.graphics.getWidth()/2)-960, (love.graphics.getHeight()/2)-540)
end

return splashScreenState
