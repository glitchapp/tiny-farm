function love.conf(t)
    t.window.icon = "assets/img/menu/icon.png"
    t.window.width = 1920
    t.window.height = 1080
    t.window.version = 0.4
    t.window.title = "Tiny Farm "..t.window.version
end
