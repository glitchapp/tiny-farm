# Tiny-Farm (fork from https://github.com/lukky-nl/Tiny-Farm)

A small farming game made in Love2d.

<img src="screenshot.webp" width=75% height=75%>

I added: 
- new assets
- music by Alexandr Zhelanov: https://opengameart.org/content/casual-game-track
- Gamepad support

I'm not the original developer and this project is unfinished so this project may never have deep gaming mechanichs... I just wanted to make the demo nicer.


